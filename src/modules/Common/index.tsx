import update from 'immutability-helper';
import { login, getLessons as getLsns } from '../../service';
import AsyncStorage from '@react-native-community/async-storage';

const TOGGLE = 'common/TOGGLE';
const UPDATE_USER = 'common/UPDATE_USER';
const UPDATE_LESSONS = 'common/UPDATE_LESSONS';
const UPDATE_CURRENT_LESSON = 'common/UPDATE_CURRENT_LESSON';
const TOGGLE_SIDEBAR = 'common/TOGGLE_SIDEBAR';
const TOGGLE_LOADER = 'common/TOGGLE_LOADER';

export function getLessons(){
  return dispatch =>
    getLsns().then(ls=>{
      dispatch({
        payload: ls,
        type: UPDATE_LESSONS,
      })
    })
}

function handleUpdateLessons(state,action){
  return update(state, {
    lessons: {
      $set: action.payload,
    },
  });
}

export function setLesson(data){
  return {
        payload: data,
        type: UPDATE_CURRENT_LESSON,
      }
}

function handleUpdateCurrentLesson(state,action){
  return update(state, {
    currentLesson: {
      $set: action.payload,
    },
  });
}

export async function loginUser(obj){
  return await login(obj)
}

export function toggleSideBar(show?){
  return {
    payload : !!show,
    type: TOGGLE_SIDEBAR,
  };
}

function handleToggleSideBar(state,action){
  return update(state, {
    show: {
      $set: action.payload,
    },
  });
}

export function updateToggle(payload) {
  return {
    payload,
    type: TOGGLE,
  };
}

function handleUpdateToggle(state, action) {
  return update(state, {
    toggle: {
      $set: action.payload,
    },
  });
}

export function setUser(usr){
  AsyncStorage.setItem('user',!!usr ? JSON.stringify(usr) : '');

  return {
        payload: usr,
        type: UPDATE_USER,
      }
}

function handleUpdateUser(state,action) {
  return update(state, {
    user: {
      $set: action.payload,
    },
  });
}


export function toggleLoader(payload) {
  return dispatch => {
    dispatch({ type: TOGGLE_LOADER, payload });
  };
}

function handleToggleLoader(state, action) {
  let newLoading = { ...state.isLoading };

  if (action.payload.value) {
    newLoading[action.payload.key] = action.payload.value;
  } else {
    delete newLoading[action.payload.key];
  }

  return update(state, {
    isLoading: {
      $set: newLoading,
    },
  });
}

const ACTION_HANDLERS = {
  [TOGGLE]: handleUpdateToggle,
  [UPDATE_USER] : handleUpdateUser,
  [TOGGLE_SIDEBAR]: handleToggleSideBar,
  [UPDATE_LESSONS]: handleUpdateLessons,
  [UPDATE_CURRENT_LESSON]: handleUpdateCurrentLesson,
  [TOGGLE_LOADER]: handleToggleLoader
};

const initialState = {
  toggle: false,
  user: null,
  show: false,
  lessons: {},
  currentLesson: {},
  isLoading: {}
};

export default function reducer(state = initialState, action){
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state 
}

