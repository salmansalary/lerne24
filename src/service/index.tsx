import xFetch from './xFetch';
import {
  InteractionManager,
} from 'react-native';
import Config from 'react-native-config';
import { store } from '../boot/setup';


export let AppContext = {
  deviceId: '',
};

export const GetAppContext = (obj?) => {
  return {
    ...obj
  };
};

export const fullURL = path => `${Config.API_URL}/${path}`;

const GETHeader = (obj?) => {
  const { common } = store.getState() || {} as any;
  const { user } = common || {} as any;
  const { accessToken } = user || {} as any;

  const headers: any = {
    "Accept": 'application/json',
    "Authorization": accessToken,
    ...GetAppContext(obj),
  };

  return headers;
};

const POSTHeader = (obj?) => {

  const { common } = store.getState() || {} as any;
  const { user } = common || {} as any;
  const { accessToken } = user || {} as any;

  let headers: any = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    "Authorization": accessToken,
    ...GetAppContext(obj),
  };
  return headers;
};

export const handleNetworkError = error => {
  let code = error.code;
  let errorCode = error.errorCode;
  let message = error.message;

  if (error.message === 'Network request failed') {
    code = 'Connection error';
    message =
      'Connection error. Please check your network settings and try again.';
  }

  //This code is when dguard callback return revalidate: true so we prevent show common alert
  if (errorCode === '8888') {
    return;
  } else {
    InteractionManager.runAfterInteractions(() => {
      setTimeout(() => {
        alert(JSON.stringify({
          title: code,
          message: message,
          type: 'error',
        }));
      }, 0);
    });
    //throw error;
  }
};

export const ServiceList: any = {
  login: 'api/auth',
  findUser: 'api/findUser',
  getLessons: ({lng,order}) => `api/sessions?lng=${lng}&$sort[order]=${order}`,
  getLesson: ({id}) => `api/sessions/${id}`,
  progress: `api/progress`
};

function silent(error) {
  return { error: error };
}

export const login = async data => {
  return await xFetch(
    fullURL(ServiceList.login),
    {
      method: 'POST',
      headers: POSTHeader(),
      body: JSON.stringify({
        ...data,
        "strategy": "local"
      }),
    },
    { handleNetworkError }
  );
};

export const findUser = async data => {
  return await xFetch(
    fullURL(ServiceList.findUser),
    {
      method: 'POST',
      headers: POSTHeader(),
      body: JSON.stringify(data)
    },
    { handleNetworkError }
  );
};

export const getLessons = () => {
  return xFetch(
    fullURL(ServiceList.getLessons({lng:'gb',order:'1'})),
    {
      method: 'GET',
      headers: GETHeader()
    },
    { handleNetworkError }
  );
};

export const getLesson = (id?) => {
  return xFetch(
    fullURL(ServiceList.getLesson({id})),
    {
      method: 'GET',
      headers: GETHeader()
    },
    { handleNetworkError }
  );
};

export const progress = async data => {
  const { _id: sessionId, percent } = data || {} as any;

  return await xFetch(
    fullURL(ServiceList.progress),
    {
      method: 'POST',
      headers: POSTHeader(),
      body: JSON.stringify({
        sessionId: sessionId,
        percent: percent
      }),
    },
    { handleNetworkError }
  );
};