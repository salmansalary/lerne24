import isEmpty from 'lodash/isEmpty';
import { toggleLoader } from '../modules/Common';
import { store } from '../boot/setup';

function snatchTheRequest(obj) {
  let res = null;
  const { path, headerOptions, ops } = obj;
  return res;
}

const checkIfErrorOccurs = res => {
  return {
    code: res.status,
    res,
  };
};

const TIME_OUT = 120000;

async function xFetch(
  path,
  headerOptions,
  ops: any = {
    noParse: false,
    handleNetworkError: () => void 0,
    timeoutMil: TIME_OUT,
  }
) {
  const cleanPath = path.split('?')[0];

  if (__DEV__) {
    console.log('___________________________________');
    console.log('Header', { url: path, ...headerOptions });
  }

  //Snatch the call and for checking
  const altRsp = snatchTheRequest({ path: cleanPath, headerOptions, ops });
  if (!isEmpty(altRsp)) {
    return altRsp;
  }
  // End of check request

  store.dispatch(toggleLoader({ key: cleanPath, value: true }));

  let res: any;
  try {
    const normalFetch = fetch(path, headerOptions);

    if (ops.noParse) {
      store.dispatch(toggleLoader({ key: cleanPath, value: false }));
      return timeoutPromise(ops.timeoutMil || TIME_OUT, normalFetch).catch(
        ops.handleNetworkError
      );
    }

    res = await timeoutPromise(ops.timeoutMil || TIME_OUT, normalFetch);

    res = checkIfErrorOccurs(res);
  } catch (er) {
    store.dispatch(toggleLoader({ key: cleanPath, value: false }));
    ops.handleNetworkError(er);
    if (__DEV__) {
      console.log('responseData Error', {
        path: cleanPath,
        er,
      });
      console.log('___________________________________');
    }
    return { data: null };
  }

  const contentType = !!res && !!res.res && res.res.headers.get('content-type');

  if (contentType && (contentType.includes('application/json'))) {
    let responseData;

    responseData = !!res && (await res.res.json());

    if (__DEV__) {
      console.log('responseData', { responseData, path });
      console.log('___________________________________');
    }

    store.dispatch(toggleLoader({ key: cleanPath, value: false }));

    if (!!res && res.code <= 500) {
      if (
        responseData.error &&
        (responseData.error.errorCode || responseData.error.statusCode)
      ) {
        if (!!responseData.error.sessionTimeout) {
          console.log('Your Session Expiered', responseData.error);
        } else {
          ops.handleNetworkError(new Error(responseData.error.message));
        }
      }
      //Disable Sucess Message
      // else if (
      //   responseData &&
      //   responseData.data &&
      //   responseData.data.successMessage
      // ) {
      //   showSuccessMessage(responseData.data.successMessage);
      // }
    } else {
      ops.handleNetworkError(new Error(responseData.message));
    }
    return responseData;
  } else {
    if (__DEV__) {
      console.log('responseData Error', res);
      console.log('___________________________________');
    }
    store.dispatch(toggleLoader({ key: cleanPath, value: false }));
    ops.handleNetworkError(
      new Error('System maintenance in progress, please try after some time!')
    );

    return { data: null };
  }
}

export const timeoutPromise = function timeoutPromise(ms, promise) {
  return new Promise((resolve, reject) => {
    let timeoutId;
    if (!__DEV__ || true) {
      timeoutId = setTimeout(() => {
        reject(new Error('request time out'));
      }, ms);
    }
    setTimeout(() => {
      promise.then(
        res => {
          if (!__DEV__) {
            clearTimeout(timeoutId);
          }
          resolve(res);
        },
        err => {
          if (!__DEV__) {
            clearTimeout(timeoutId);
          }
          reject(err);
        }
      );
    }, 0);
  });
};

export default xFetch;
