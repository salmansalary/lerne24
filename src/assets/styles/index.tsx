const styles: any = {
    colors:{
      white1:'#ffffff',
      blue1: '#3d5b9c',
      green1:'#007881',
      grey1: '#3A4144',
      grey2: '#869099',
      grey3: '#cdd1d4',
      grey4: '#c6c5c5',
      grey5: '#efefef',
      grey6: '#bfbdc2',
      orange1: '#ffb40f',
    }
  };
  
  export default styles;
  