/* eslint-disable @typescript-eslint/no-unused-vars */

import { store } from '../boot/setup';

// import { request, PERMISSIONS } from 'react-native-permissions';
// import { PermissionsAndroid,Platform } from 'react-native';
// import AsyncStorage from "@react-native-community/async-storage";

export const dispatch = func => {
  store && store.dispatch(func);
};
export default {};
