import { applyMiddleware, createStore } from 'redux';
import { persistStore } from 'redux-persist';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import reducer from '../reducers';

export default function configureStore(onCompletion: () => void): any {
  const enhancer = __DEV__ ? composeWithDevTools(applyMiddleware(thunk)) : applyMiddleware(thunk);

  const store = createStore(reducer, enhancer);
  persistStore(store, undefined, onCompletion);

  return store;
}
