/**
 * @format
 */
import { AppRegistry } from 'react-native';
import boot from './boot/index';

const app = boot();
AppRegistry.registerComponent('starter', () => app);
