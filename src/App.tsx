import React, { Fragment } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  BackHandler,
  AppState
} from 'react-native';

import { connect } from 'react-redux';

import styled from 'styled-components';

import { SideBar } from './components/SideBar';
import { Login } from './screens/Login';
import { Dashboard } from './screens/Dashboard';
import { Lesson } from './screens/Lesson';

import SplashScreen from 'react-native-splash-screen';
import { Wrapper } from './components/Wrapper';
import WebView from './components/WebViewWrapper';

import AsyncStorage from '@react-native-community/async-storage';
import { setUser } from './modules/Common';
import isEmpty from 'lodash/isEmpty';

const SScrollView = styled(ScrollView)`
  background-color: white;
  flex: 1;
`;

const SSafeAreaView = styled(SafeAreaView)`
  /* background-color: #007882; */
`;

export interface State {
  Screen?: JSX.Element;
  ScreenObj?: any;
  params?: any;
  stackObj?: any;
}

const Screens = [
  {
    name : 'Login',
    Component : Login
  },
  {
    name : 'Dashboard',
    Component : Dashboard,
    header: true,
    footer: false
  },
  {
    name : 'WebView',
    Component : WebView,
    header: false,
    footer: false
  },
  {
    name : 'Lesson',
    Component : Lesson,
    header: true,
    footer: true
  }
];

class App extends React.Component<any,State>{
  backHandler;

  constructor(props){
    const dScreen = Screens[0];
    const { name, Component } = dScreen || {} as any;
    super(props);
    this.state = {
      params: {},
      stackObj: [name],
      ScreenObj : dScreen,
      Screen: Component
    };
    this.navigate = this.navigate.bind(this);
    this.handleBack = this.handleBack.bind(this);
    this.handleAppStateChange = this.handleAppStateChange.bind(this);
    if (__DEV__) {
      console.disableYellowBox = false;
    }
  }

  handleBack(){
    const { ScreenObj } = this.state;
    const { name } = ScreenObj || {} as any;

    if (name === 'Dashboard' || name === 'Login'){
      BackHandler.exitApp();
      return false;
    } else {
      this.navigate(null, {action : 'goBack'});
      return true;
    }
  }

  handleAppStateChange(nextAppState) {
    if (nextAppState.match(/inactive|background/)) {

      // AppState.removeEventListener('change', this.handleAppStateChange);
    }
  }

  componentDidMount(){
    if (__DEV__) {
      console.disableYellowBox = true;
    }

    AsyncStorage.getItem('user').then(user=>{
      const usr = !isEmpty(user) ? JSON.parse(user) : {};
      const { accessToken } = usr || {} as any;

      if(!isEmpty(accessToken)){
        this.props.setUser(usr);
        this.navigate('Dashboard',null);
      }
    });
    
    
    SplashScreen.hide();
    this.backHandler = BackHandler.addEventListener('hardwareBackPress',this.handleBack);

    AppState.addEventListener('change', this.handleAppStateChange);

    StatusBar.setBackgroundColor('white');
    StatusBar.setBarStyle('dark-content')
  }

  componentWillUnmount(){
    this.backHandler.remove();
  }

  navigate(trg,nav){
    let stackObj = this.state.stackObj;
    const length = stackObj.length;
    const { action, ...params } = nav || {} as any;

    let target = trg;

    if (action === 'goBack'){
      if (length > 1){
        target = stackObj.pop();
        target = stackObj.slice(-1)[0];
      }
    }

    const screen = Screens.find(sc => sc.name === target) || Screens[0];
    const { Component, name } = screen || {} as any;

    stackObj = stackObj.filter(scr=> scr !== name);
    stackObj = [...stackObj,name];
    this.setState({ Screen : Component, params: {...params }, ScreenObj: screen, stackObj });
  }

  render(){
    SplashScreen.hide();
    //@ts-ignore
    const { Screen, params, ScreenObj } = this.state || {} as any;
    const { header, footer, name } = ScreenObj || {} as any;
    const paramWrapper = {
      ...params,
      navigation: {
        navigate : this.navigate,
        goBack : ()=>this.navigate(null,{ action: 'goBack' })
    }};

    return (
      <Fragment>
        <StatusBar 
          // backgroundColor="#007882" 
          barStyle="dark-content"
          />
        <SSafeAreaView>
        
        <Wrapper header={header} footer={footer} navigation={paramWrapper.navigation} screen={name}>
            <SScrollView contentInsetAdjustmentBehavior="automatic">
              {
                //@ts-ignore
                <Screen {...paramWrapper}/>
              }
            </SScrollView>
          </Wrapper>
        </SSafeAreaView>
        <SideBar {...paramWrapper}/>
      </Fragment>
    );
  }
}

const mapDispatchToProps =  dispatch => { return {
  setUser: (user) => dispatch(setUser(user))
}};

const mapPropstoProps = state => ({
});

const AppContainer =  connect(
    mapPropstoProps,
    mapDispatchToProps
)(App);

export default AppContainer;
