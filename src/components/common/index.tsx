
import { 
    KeyboardAvoidingView,
    Dimensions,
    View,
    Text,
} from "react-native";
import { Button, Content } from 'native-base';
import styled from "styled-components";
import Assets from '../../assets/index';
import Styles from '../../assets/styles';

const ContentWrapper = styled(KeyboardAvoidingView)`
  background-color: transparent;
  margin-top: 0;
  margin-bottom: 0;
  flex: 1;
  padding: 0;
`;

const { 
    width: deviceWidth, 
    height: deviceHeight 
  } = Dimensions.get('window');
  
const SView = styled(View)`
width: ${deviceWidth};
height: ${deviceHeight};
`;

const SContent = styled(Content)`
padding: 20px;
flex: 1;
background-color: transparent;
`;

const Row = styled(View)`
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: ${ props => props.align || 'flex-start'};
`;

const SButton = styled(Button)`
  left: 0;
  border-width: 1px;
  border-color: ${props => !!props.borderColor ? props.borderColor : Styles.colors.green1};
  border-radius: 7;
  height: 42;
  justify-content: center;
  width: auto;
  padding: 0 15px;
  background-color: ${props => props.bgColor || Styles.colors.green1};
`;

const Label = styled(Text)`
  color: ${props => props.color || Styles.colors.white1};
  font-size: ${props => props.fontSize || 16};
`;

const SemiBoldLabel = styled(Label)`
  font-weight: 500;
`;

const BoldLabel = styled(Label)`
  font-weight: bold;
`;


export {
    ContentWrapper,
    SView,
    SContent,
    Row,
    SButton,
    Label,
    SemiBoldLabel,
    BoldLabel
}