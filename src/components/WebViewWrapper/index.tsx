import React from 'react';
import {
  View,
  Platform,
  KeyboardAvoidingView,
  TouchableOpacity,
  SafeAreaView,
  Dimensions,
  Text,
  StatusBar
} from 'react-native';
import styled from 'styled-components/native';
import { connect } from 'react-redux';
import WebView from 'react-native-webview';
import Config from 'react-native-config';
import { store } from '../../boot/setup';
import { Icon } from 'native-base';
import styles from '../../assets/styles';

export const fullURL = path => `${Config.API_URL}/${path}`;

const { width, height} = Dimensions.get('window') || {} as any;

export interface Props {
  isLoading?: boolean;
  footer?: any;
  onNavigationStateChange?: any;
  injectAddon?: any;
  url?: any;
  keyboardBehaviour?: any;
  navigation: any;
}

export interface States {
  jsToInject?: any;
  sniff?: any;
  source?: any;
}

const SlViewWrapper = styled(View)`
  flex: 1;
  background-color: transparent;
`;


const TButton = styled(TouchableOpacity)`
  height: 50;
  width: 70;
  align-items: center;
  justify-content: center;
`;

const HeaderView = styled(View)`
  top: 0;
  left: 0;
  height: 50;
  width: ${width};
  flex-direction: row;
  justify-content: flex-start;
  background-color: #fff;
  align-items: center;
  border-bottom-width: 1;
  border-bottom-color: ${styles.colors.grey5};
`;

const TitleView = styled(View)`
  background-color: #fff;
  height: 50;
  width: ${width - 140};
  flex-direction: row;
  justify-content: center;
`;

const Title = styled(Text)`
  font-family: "Philosopher-BoldItalic";
  height: 50;
  line-height: 50;
  font-size: 20;
  color: white;
`;

class WebViewWrapper extends React.PureComponent<Props, States> {
    webview;
    constructor(props) {
        super(props);
        this.state = {
        jsToInject: '',
        sniff: false,
        source: null
        };
    }

    componentWillMount() {
      const { common } = !!store && store.getState();
      const { user } = common || {} as any;
      const { accessToken } = user || {} as any;

        this.setState({
          source : {
            uri : `https://www.realistig.com/session?id=${this.props['_id']}&deviceoutput=app`,
            headers: {
              Cookie: `lng=gb; feathers-jwt=${accessToken}`
            },
          }
      })
    }

  render() {
    const {
      footer,
      onNavigationStateChange,
      keyboardBehaviour,
      ...restof
    } = this.props;
    const {  source } = this.state;


    let newProps = {
          ...restof,
          source
        } as any;

    if (Platform.OS === 'android') {
      newProps = {
        ...newProps,
        pointerEvents: 'none',
        mixedContentMode: 'always',
        saveFormDataDisabled: true,
        setSavePassword: false,
        setAppCacheEnabled: false,
        setCacheMode: 2,
      };
    }

    const saveViewProps = {
      style: {
        flex: 1,
      },
    } as any;

    const less = ((Platform.OS === 'ios' ? 20 : 24));
    const screenHeight = height - less;
    return (
      <KeyboardAvoidingView
        behavior={!!keyboardBehaviour ? keyboardBehaviour : 'height'}
        style={{
          width: '100%',
          height: screenHeight,
          // maxHeight: '100%',
          // minHeight: '100%',
          backgroundColor: '#dcdcdc'
        }}
      >
        <SafeAreaView {...saveViewProps}>
          <HeaderView>
            <TButton onPress={() => this.props.navigation.goBack()}>
              <Icon style={{ fontSize: 40, color: '#007882' }} name="close" />
            </TButton>
            <TitleView>
              <Title>lerne24</Title>
            </TitleView>
          </HeaderView>
          <SlViewWrapper
            style={{
              padding: 0,
              width: '100%',
              height: screenHeight - 50,
              overflow: 'hidden'
            }}
          >
            <WebView
              ref={c => (this.webview = c)}
              javaScriptEnabled={true}
              domStorageEnabled={true}
              startInLoadingState={false}
              automaticallyAdjustContentInsets={true}
              scrollEnabled={true}
              ignoreSslError={true}
              scalesPageToFit={true}
              sharedCookiesEnabled={true}
              //@ts-ignore
              onNavigationStateChange={async webViewState => {
                //@ts-ignore
                // if (!webViewState.loading) {
                console.log('WebView Url Changed', webViewState);
                // }
                const { url } = webViewState;
                if(url.includes('realistig.com')){
                  if(!url.includes('session') || url.includes('lng') || url.includes('sessions')){
                    this.props.navigation.goBack();
                  }
                }
                
              }}
              {...newProps}
              enableCache={true}
              style={{
                flex: 1,
                width: '100%',
                overflow: 'scroll',
                backgroundColor: '#dcdcdc',
              }}
            />
          </SlViewWrapper>
        </SafeAreaView>
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = (state, props) => ({

});

export default connect(mapStateToProps, null)(WebViewWrapper);
