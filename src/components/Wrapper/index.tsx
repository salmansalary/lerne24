import React from 'react';
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  Text,
  Animated,
  SafeAreaView
} from 'react-native';
import { Icon } from 'native-base';
import { BoldLabel, SButton, SemiBoldLabel } from "../../components/common";
import { connect } from 'react-redux';
import styled from 'styled-components';
import { toggleSideBar } from '../../modules/Common';
import Assets from '../../assets';
import CommonLoader from '../../components/CommonLoader';
import styles from '../../assets/styles';

const { width: deviceWidth, height: deviceHeight } = Dimensions.get('window');

const SView = styled(View)`
  width: ${deviceWidth};
  height: 100%;
`;

const TButton = styled(TouchableOpacity)`
  height: 50;
  padding-left: 20px;
  padding-right: ${(deviceWidth * 5)/100};
  align-items: center;
  justify-content: center;
`;

const HeaderView = styled(View)`
  background-color: #fff;
  top: 0;
  left: 0;
  height: 70;
  width: ${deviceWidth};
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  border-bottom-width: 1;
  border-bottom-color: ${styles.colors.grey5};
`;

const TitleView = styled(View)`
  /* background-color: #007882; */
  height: 50;
  width: ${deviceWidth - 140};
  flex-direction: row;
  justify-content: center;
`;

const Title = styled(Text)`
  font-family: "Philosopher-BoldItalic";
  height: 50;
  line-height: 50;
  font-size: 20;
  /* color: white; */
`;

const FooterView = styled(View)`
  position: absolute;
  bottom: 0;
  left: 0;
  height: 70;
  width: ${deviceWidth};
  z-index: 10;
  border-top-width: 1;
  border-top-color: #cfcfcf;
  flex-direction: row;
  justify-content: flex-start;
  background-color: white;
  border-color: white;
  border-Width: 1;
`;

export interface Props {
  navigation?: any;
  toggleSideBar: Function;
  children?: JSX.Element;
  header?: boolean;
  footer?: boolean;
  user?: any;
  screen?: string;
}


class WrapperComponent extends React.Component<Props, any> {
  animatedValue;
  viewInterpolate;
  val;
  constructor(props) {
    super(props);
    this.animatedValue = new Animated.Value(0);
    this.val = 0;
    const radio = deviceWidth / 72;

    this.viewInterpolate = [
      {
        translateY: this.animatedValue.interpolate({
          inputRange: [0, 1],
          outputRange: [0, ((18 * radio) / 4 + 50) * -1]
        })
      },
      {
        scaleX: this.animatedValue.interpolate({
          inputRange: [0, 1],
          outputRange: [1, radio]
        })
      },
      {
        scaleY: this.animatedValue.interpolate({
          inputRange: [0, 1],
          outputRange: [1, radio]
        }),
      },
    ];
  }

  expand(va) {
    Animated.spring(this.animatedValue, {
      toValue: va === 1 ? 0 : 1,
      velocity: 1,
      friction: 3,
      useNativeDriver: true
    }).start();
    this.val = va === 1 ? 0 : 1;
  }

  handleSubmit() {
    const { navigation } = this.props || ({} as any);
  }

  flagRenderer(lang: string){
    switch (lang) {
      case "de": return Assets.common.germanyFlag();
      case "en": return Assets.common.britainFlag();
      default:
        break;
    }
  }


  render() {
    const { navigation, header, footer, children, user, screen } = this.props;
    let height = deviceHeight - (header ? 50 : 0);
    height = height - (footer ? 60 : 0);

    const footerObj = [
      {
        icon: Assets.common.home(),
        name: 'Dashboard',
        onPress: () => navigation.navigate('Dashboard')
      },
    ];

    const centerGap = (deviceWidth * 350) / 1521;
    const wrapperWidth = (deviceWidth - centerGap) / 2;


    
    return (
        <SView
          style={{
            width: deviceWidth,
          }}
        >
          {!!header && (
            <HeaderView>
              <TButton onPress={() => this.props.toggleSideBar(true)}>
                <Image source={Assets.common.menuIcon()}/>
              </TButton>
              <SButton 
                style={{}} 
                bgColor={styles.colors.white1}
                borderColor={styles.colors.grey3}
                onPress={() => navigation.navigate('Dashboard')}
                >
                  <Image style={{width: 100, height: 14}} source={Assets.common.logo()}/>
                  <Image style={{marginLeft: 8}} source={this.flagRenderer(this.props.user && this.props.user.toLearn && this.props.user.toLearn.code || "de" as string)}/>
              </SButton>
              <SButton 
                style={{marginLeft: 20}} 
                bgColor={styles.colors.white1}
                borderColor={styles.colors.grey3}
                activeOpacity={1}
                >
                  <BoldLabel color={styles.colors.grey1}>LEVEL {this.props.user && this.props.user.toLearn && this.props.user.toLearn.level.toUpperCase() || "A1" as string}</BoldLabel>
              </SButton>
            </HeaderView>
            
          )}
          <SView style={{ flex: 1 }}>{children}</SView>
          {!!footer && (
            <FooterView>
              <View
                style={{
                  width: wrapperWidth,
                  height: 70,
                  flexDirection: 'row'
                }}
              >
                {footerObj.map((obj, index) => {
                  return (
                    <SButton
                      key={index}
                      onPress={obj.onPress}
                      style={{
                        width: wrapperWidth / 2
                      }}
                    >
                      {
                        //@ts-ignore
                        <Image
                          resizeMode="contain"
                          style={{
                            width: 24,
                            height: 24,
                            opacity: obj.name === screen ? 1 : 0.4,
                          }}
                          source={obj.icon}
                        />
                      }
                    </SButton>
                  );
                })}
              </View>
            </FooterView>
          )}
        <CommonLoader />
        </SView>
    );
  }
}

const mapDispatchToProps = {
  toggleSideBar
};

const mapPropstoProps = state => ({
  user: state.common.user
});

const Wrapper = connect(
  mapPropstoProps,
  mapDispatchToProps
)(WrapperComponent);

export { Wrapper };
