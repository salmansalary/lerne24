import React from 'react';
import { connect } from 'react-redux';
import { View, Dimensions } from 'react-native';
import { Spinner } from 'native-base';
import styled from 'styled-components/native';
import isEmpty from 'lodash/isEmpty';
import { ServiceList } from '../../service';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
export interface Props {
  isLoading?: any;
  commonLoader?;
  any;
  fullScreen?: boolean;
}

const LoaderView = styled(View)`
  /* background-color: rgba(0,0,0,0.4); */
  background-color: transparent;
  width: ${deviceWidth};
  position: absolute;
  justify-content: center;
  align-items: center;
  padding: 0;
  margin: 0;
  left: 0;
`;

const SSpinner = styled(Spinner)`
  background-color: transparent;
  width: 60;
  height: 60;
`;
class CommonLoader extends React.PureComponent<Props, {}> {
  constructor(props) {
    super(props);
  }
  render() {
    const { commonLoader, isLoading, fullScreen } = this.props;
    const excludeSrv = [

    ];

    const loaderStack = Object.keys(commonLoader).filter(
      srv => !excludeSrv.find(ex => srv.includes(ex))
    );

    const isVisible = !isEmpty(loaderStack) || isLoading;

    const customStyle = {
      bottom: fullScreen ? 0 : 60,
      paddingBottom: fullScreen ? 60 : 0,
      height: deviceHeight - (fullScreen ? 120 : 180),
    };

    return isVisible ? (
      <LoaderView style={customStyle}>
        <SSpinner size="large" color="#000000" />
      </LoaderView>
    ) : null;
  }
}

const mapStateToProps = state => ({
  commonLoader: state.common.isLoading || {},
});

export default connect(
  mapStateToProps,
  null
)(CommonLoader);
