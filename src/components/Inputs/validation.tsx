

const required = (fieldName) => value =>
  value ? undefined : `Please enter ${fieldName}`;

const maxLength = (max, customMessage?) => value =>
  value && value.length > max
    ? customMessage || `Must be ${max} characters or less`
    : undefined;

const minLength = (min, customMessage?) => value =>
  value && value.length < min
    ? customMessage || `Must be ${min} characters or more`
    : undefined;

const numberic = value =>
  value && /[^0-9]/.test(value) ? 'Only numberic characters' : undefined;
const mobileNumber = value =>
  value && (numberic(value) || value.length < 9)
    ? 'Please enter a valid mobile number'
    : undefined;

const altMobileNumber = value =>
  value && (numberic(value) || value.length < 10)
    ? 'Please enter a valid mobile number'
    : undefined;

const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(value)
    ? 'Invalid email address'
    : undefined;


const amount = (min) => value =>
  value < min
    ? `Minimum amount is ${min}.00 $`
    : undefined;

const maxAmount = (max) => value => 
  parseFloat(value
  .replace(/,/gi, '')) > max
    ? `Maximum amount is ${max} $`
    : undefined;

export {
  required,
  maxLength,
  minLength,
  mobileNumber,
  altMobileNumber,
  email,
  amount,
  maxAmount
};
