import React from 'react';
import {
  Dimensions,
  View,
  SafeAreaView,
  KeyboardAvoidingView,
  Image,
  Text,
  TouchableWithoutFeedback,
  Alert,
  TouchableOpacity
} from 'react-native';
import { Button, Content } from 'native-base';
import { 
  ContentWrapper,
  SButton,
  Label,
  SemiBoldLabel,
  BoldLabel,
  SView,
  SContent,
 } from "../../components/common";
import { CustomInput } from '../../components//Inputs';
import { reduxForm, Field, change, reset } from 'redux-form';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { loginUser, setUser } from '../../modules/Common';
import { required, email } from '../../components/Inputs/validation';
import { findUser } from '../../service';

import Assets from '../../assets/index';
import Styles from '../../assets/styles';
import isEmpty from 'lodash/isEmpty';

const requiredEmail = required('your Email address');
const requiredPassword = required('Password');

const TopView = styled(View)`
  flex: 1;
`;

const DeviderContainer = styled(View)`
  height: 40px;
  justify-content: center;
`;
const DeviderLine = styled(View)`
  border-bottom-color: ${Styles.colors.grey3};
  border-bottom-width: 1px;
  width: 100%;
  align-items: center;
`;

const DeviderLabel = styled(Text)`
  position: absolute;
  background-color: ${Styles.colors.white1};
  top: -8px;
  width: 30px;
  text-align: center;
  color:${Styles.colors.grey2};
  font-weight: bold;
`;

export interface Props {
  navigation?: any;
  handleSubmit: Function;
  changeFieldValue: Function;
  resetForm: Function;
  login?: any;
  loginUser?: any;
  setUser?: any;
  user?: any;
}

export interface States {
  showPassword: boolean
}

class LoginFrom extends React.Component<Props, States> {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.checkUser = this.checkUser.bind(this);
    this.state = {
      showPassword: false
    }
  }

  async checkUser(email?) {
    const resp = await findUser({ email });
    const { user, message } = resp || {} as any;

    if(!isEmpty(user)){
      this.setState({showPassword: true})
    } else if(!isEmpty(message)){
      alert(message)
    }
  }

  async handleSubmit() {
    const { login, navigation, resetForm } =
      this.props || ({} as any);

    const { values } = login || ({} as any); // login is the form
    const { email, password } = values || {} as any;

    if(!this.state.showPassword) {
      this.checkUser(email)
      return;
    }

    const resp = await loginUser({ email, password });
    const { accessToken, user } = resp || {} as any;
    
    if(!isEmpty(accessToken) && !isEmpty(user)){
      this.props.setUser({
        ...user,
        accessToken
      })
      setTimeout(() => {
        navigation.navigate('Dashboard');
      }, 500);
    } else {
      setUser(null)
    }

  }

  handleFacebookLogin(){
    return Alert.alert("Facebook Login!")
  }

  render() {
    const inputStyle = {
      borderRadius: 25,
      height: 50,
      backgroundColor: Styles.colors.grey3,
      flexDirection: 'row',
      justifyContent: 'flex-start'
    };
    const { login, user } = this.props || ({} as any);
    const { email: em } = user || ({} as any);
    const inValid = login ? !isEmpty(login.syncErrors) : true;

    let props = {} as any;
    if (inValid) {
      props.warning = true;
      props.onPress = () => {};
    } else {
      props.primary = true;
    }

    const styleProps = {
      style: {
        //   backgroundColor: '#007882'
      }
    };

    
    return (
      <SafeAreaView {...styleProps}>
        <SView>
          <ContentWrapper behavior="padding">
            <SContent
              contentContainerStyle={{
                flex: 1,
                justifyContent: 'space-between'
              }}
            >
              <View style={{width: '100%', flexDirection: 'row-reverse'}}>
                <TouchableWithoutFeedback onPress={()=> Alert.alert("Close")}>
                  <Image source={Assets.common.corssBlack()}/>
                </TouchableWithoutFeedback>
              </View>
              <View style={{width: '100%', alignItems: 'center', marginTop: 25}}>
                <Image source={Assets.common.logo()}/>
                <Text style={{marginTop: 40, fontSize: 18}}>Sign up or Log in</Text>
              </View>
              <TopView style={{marginTop: 40}}>
                {
                  !this.state.showPassword ?
                  <Field
                  label={'E-mail'}
                  name="email"
                  component={CustomInput}
                  // placeholder={"user@email.com"}
                  autoCapitalize={'none'}
                  inputStyle={inputStyle}
                  validate={[requiredEmail, email]}
                  autoCorrect= {false}
                  defaultValue={em}
                  /> 
                  :
                  <>
                    <Field
                      label={'Password'}
                      name="password"
                      component={CustomInput}
                      // placeholder={"XXXXXXX"}
                      inputStyle={inputStyle}
                      validate={[requiredPassword]}
                      autoCapitalize={'none'}
                      autoCorrect= {false}
                      password={true}
                    />
                    <TouchableOpacity 
                      style={{marginTop: -5}}
                      onPress={()=> Alert.alert("Forgot Password")}>
                      <SemiBoldLabel color={Styles.colors.green1}>Forgot Password</SemiBoldLabel>
                    </TouchableOpacity>
                  </>
                }
                <View style={{marginTop: 25}}>
                  <SButton 
                    bgColor={this.state.showPassword ? Styles.colors.white1 : Styles.colors.green1}
                    borderColor={Styles.colors.green1}
                    onPress={this.handleSubmit}
                    >
                    <BoldLabel color={this.state.showPassword ? Styles.colors.green1 : Styles.colors.white1}>Submit</BoldLabel>
                  </SButton>
                  <DeviderContainer>
                    <DeviderLine>
                      <DeviderLabel>Or</DeviderLabel>
                    </DeviderLine>
                  </DeviderContainer>
                  <SButton 
                    bgColor={Styles.colors.blue1} 
                    style={{marginTop: 20}}
                    onPress={this.handleFacebookLogin}
                    >
                    <Image source={Assets.common.fbLogoWhite()} style={{position: 'absolute', left: 10}}/>
                    <BoldLabel>Continue With Facebook</BoldLabel>
                  </SButton>
                </View>

                
                
              </TopView>
              {
                //@ts-ignore
                // <SButton onPress={this.handleSubmit} {...props}>
                //   <Text> Login </Text>
                // </SButton>
              }
            </SContent>
          </ContentWrapper>
        </SView>
      </SafeAreaView>
    );
  }
}

const LoginContainer = reduxForm({
  destroyOnUnmount: false,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
  form: 'login'
})(LoginFrom);

const mapDispatchToProps = dispatch => { return {
  changeFieldValue: obj=> dispatch(change(obj)),
  resetForm: obj=> dispatch(reset(obj)),
  loginUser: obj=> dispatch(loginUser(obj)),
  setUser: obj => dispatch(setUser(obj))
}};

const mapPropstoProps = state => ({
  login: state.form.login,
  initialValues: state.common.user,
  user: state.common.user
});

const Login = connect(
  mapPropstoProps,
  mapDispatchToProps
)(LoginContainer);

export { Login };
