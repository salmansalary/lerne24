import React, { Fragment } from 'react';
import { Dimensions,
  View,
  SafeAreaView, 
  KeyboardAvoidingView
} from 'react-native';
import { Content } from 'native-base';

import { connect } from 'react-redux';
import styled from 'styled-components';


const { width : deviceWidth, height: deviceHeight } = Dimensions.get('window');

const SView = styled(View)`
  width: ${deviceWidth};
  height: ${deviceHeight};
`;

const ContentWrapper = styled(KeyboardAvoidingView)`
  background-color: transparent;
  margin-top: 0;
  margin-bottom: 0;
  flex: 1;
  padding: 0;
`;

const SContent = styled(Content)`
  padding: 20px;
  flex: 1;
  background-color: transparent;
`;


export interface Props {
  navigation?: any;
}


class TemplateComponent extends React.Component<Props,any>{

    constructor(props){
      super(props);
    }
    

    render(){
      const { } = this.props || {transactions: [],merchantTransactions: []} as any;
     

      return (
        <SafeAreaView>
          <ContentWrapper  behavior="padding">
            <SContent 
                scrollEnabled={false}
                disableKBDismissScroll={true}
                contentContainerStyle={{
                  justifyContent: 'space-between',
                  backgroundColor: 'transparent',
                  flex: 1,
                }}
            >
                
            </SContent>
          </ContentWrapper>
      </SafeAreaView>
      );
    }
}

const mapDispatchToProps = null;

const mapPropstoProps = state => ({
});

const Template =  connect(
    mapPropstoProps,
    mapDispatchToProps
)(TemplateComponent);

export { Template };

