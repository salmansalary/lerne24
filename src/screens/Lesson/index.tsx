import React, { Fragment } from 'react';
import { Dimensions,
  View,
  SafeAreaView, 
  KeyboardAvoidingView,
  TouchableOpacity
} from 'react-native';
import { Content, Icon } from 'native-base';

import { connect } from 'react-redux';
import styled from 'styled-components';
import isEmpty from 'lodash/isEmpty';
import HTML from 'react-native-render-html';
import Config from 'react-native-config';
import SoundPlayer from 'react-native-sound-player'


export const getVoiceURL = path => `${Config.API_URL}/voice/${path}`;

const { width : deviceWidth, height: deviceHeight } = Dimensions.get('window');


const ContentWrapper = styled(KeyboardAvoidingView)`
  background-color: transparent;
  margin-top: 0;
  margin-bottom: 0;
  flex: 1;
  padding: 0;
`;

const SContent = styled(Content)`
  padding: 20px 0;
  flex: 1;
  height: ${deviceHeight - 130};
  background-color: transparent;
`;



const Title = styled.Text`
  font-weight: bold;
  font-size: 16;
  margin-bottom: 5;
`;

const PicView = styled.View`
  
`;


const Pic = styled.Image`
  width: ${deviceWidth - 20};
  height: 200;
  border-color: lightgray;
  border-width: 1;
  border-radius: 7;
  margin-left: 10;
  resize-mode: cover;
  margin-bottom: 5;
`;


const CopyRight = styled.Text `
    font-weight: bold;
    color: white;
    textShadowColor: 'rgba(0, 0, 0, 1)';
    textShadowOffset: {width: -2; height: 2};
    textShadowRadius: 2;
    font-size: 15;
    position: absolute;
    top: 20;
    left: 30;
    z-index: 2;
`;

const TextSection = styled.View `
  margin: 0 10px;
  width: ${deviceWidth - 100};
`;

const BtnView = styled.View`
  position: absolute;
  left: 0;
  bottom: 0;
  width: 100%;
  height: 80;
  padding: 0 20px;
  flex-direction: row;
  justify-content: space-between;
`;  

const Voice = styled(TouchableOpacity)`
  height: 60;
  width: 60;
  justify-content: center;
  align-items: center;
  border-radius: 30;
  border-color: gray;
  border-width: 0;
  background-color: #00adef;
  overflow: hidden;
  position: absolute;
  left: 20;
  bottom: 20;
`;

const Btn = styled(TouchableOpacity)`
  height: 60;
  width: 60;
  justify-content: center;
  align-items: center;
  border-radius: 30;
  border-color: gray;
  border-width: 2;
  overflow: hidden;
`;

export interface Props {
  navigation?: any;
  currentLesson?: any;
}


class LessonComponent extends React.Component<Props,any>{

    constructor(props){
      super(props);
      this.state = {
        questions:[],
        currentQuestion: {}
      }
      this.showQuestion = this.showQuestion.bind(this);
    }
    
    // Create instance variable(s) to store your subscriptions in your class
    _onFinishedLoadingURLSubscription = null

    componentDidMount() {
      
      this._onFinishedLoadingURLSubscription = SoundPlayer.addEventListener('FinishedLoadingURL', ({ success, url }) => {
        console.log('finished loading url', success, url)
        SoundPlayer.play();
      })

      const { currentLesson } = this.props || {} as any;
      const { questions } = currentLesson || {} as any;
      this.setState({
        questions,
        currentQuestion: !isEmpty(questions) ? questions[0] : {},
        currentIndex: 0
      })

      const cq = !isEmpty(questions) ? questions[0] : {}
      const { hasVoice, audio } = cq || {} as any;
      const voice = hasVoice ? getVoiceURL(audio) : null;
      if(voice) SoundPlayer.playUrl(voice);
    }

    componentWillUnmount() {
      this._onFinishedLoadingURLSubscription.remove()
    }

    showQuestion(forward?) {
      const { currentIndex, questions } = this.state || {};
      const length = !isEmpty(questions) ? questions.length : 0;
      let id = forward ? (
        currentIndex + 1 < length ?
        currentIndex + 1 :
        length
      ) :
      (
        currentIndex > 0 ?
        currentIndex - 1 :
        0 
      );

      this.setState({
        currentIndex: id,
        currentQuestion: questions[id]
      })

      const cq = questions[id]
      const { hasVoice, audio } = cq || {} as any;
      const voice = hasVoice ? getVoiceURL(audio) : null;
      if(voice) SoundPlayer.playUrl(voice);
    }

    render(){
      const { currentQuestion: cq } = this.state || {};
      const { hasVoice, audio } = cq || {} as any;
      const voice = hasVoice ? getVoiceURL(audio) : null;

      return (
        <SafeAreaView>
          <ContentWrapper  behavior="padding">
            <SContent 
                scrollEnabled={false}
                disableKBDismissScroll={true}
                contentContainerStyle={{
                  justifyContent: 'space-between',
                  backgroundColor: 'transparent',
                  flex: 1,
                  height: '100%'
                }}
            >
              {
                !isEmpty(cq) &&
                <View>
                  {
                    !!cq.copyrights &&
                    <CopyRight>{cq.copyrights}</CopyRight>
                  }
                  <PicView>
                    {
                      !!cq.image &&
                      <Pic 
                        source={{uri: cq.image}} 
                      />
                    }
                    {
                      !!voice &&
                      <Voice
                        onPress={()=>{
                          try {
                            if(voice) SoundPlayer.playUrl(voice);
                          } catch (er) {
                              console.log({er})
                          }
                        }}
                      >
                        <Icon style={{ fontSize: 30, color: 'white', left: 2 }} name="play"  />
                      </Voice>

                    }
                  </PicView>
                  <TextSection>
                    <Title>{cq.question}</Title>
                    <HTML html={cq.description}/>
                  </TextSection>
                </View>
              }
              <BtnView>
                <Btn onPress={()=>this.showQuestion()}>
                  <Icon style={{ fontSize: 30 }} name="arrow-back" />
                </Btn>
                <Btn onPress={()=>this.showQuestion(true)}>
                  <Icon style={{ fontSize: 30 }} name="arrow-forward" />
                </Btn>
              </BtnView>
            </SContent>
          </ContentWrapper>
      </SafeAreaView>
      );
    }
}

const mapDispatchToProps = null;

const mapPropstoProps = state => ({
  currentLesson: state.common.currentLesson
});

const Lesson =  connect(
    mapPropstoProps,
    mapDispatchToProps
)(LessonComponent);

export { Lesson };

