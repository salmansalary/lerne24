
import React, { Fragment } from "react";
import { 
  SafeAreaView, 
  KeyboardAvoidingView, 
  Dimensions, 
  TouchableWithoutFeedback, 
  View,
  Image
} from "react-native";
import { Content } from "native-base";
import { connect } from "react-redux";
import styled from "styled-components";
import { getLessons, setLesson } from '../../modules/Common'
import isEmpty from 'lodash/isEmpty';
import HTML from 'react-native-render-html';
import styles from "../../assets/styles";
import Assets from "../../assets/index";
import { Label } from "../../components/common";
import Pie from 'react-native-pie'

const { width } = Dimensions.get('window');

const ContentWrapper = styled(KeyboardAvoidingView)`
  background-color: transparent;
  margin-top: 0;
  margin-bottom: 0;
  flex: 1;
  padding: 0;
`;

const SContent = styled(Content)`
  padding-top: 10px;
  flex: 1;
  background-color: transparent;
`;

const TextSection = styled.View `
  padding: 0 10px;
  flex: 1;
`;

const Item = styled.View`
  flex-direction: row;
  border-bottom-color: ${styles.colors.grey3};
  border-bottom-width: 1;
  margin: 0 12px;
  padding: 20px 0;
  /* background-color: red; */
`;  

const Title = styled.Text`
  font-weight: bold;
  font-size: 16;
  margin-bottom: 5;
`;

const Percent = styled.Text`
  font-weight: bold;
  font-size: 16;
  width: 100%;
  text-align: right;
  padding-right:10;
`;

const Pic = styled.Image`
  width: 70;
  height: 70;
  border-radius: 50;
  position: absolute;
  top: 5px;
  left: 5px;
`;

const TimeBlock = styled.View`
  width: 100px;
  height: 24px;
  background-color: ${styles.colors.grey5};
  border-color: ${styles.colors.grey6};
  border-width: 1px;
  border-radius: 4;
  flex-direction: row;
  padding: 0 5px;
  align-items: center;
`;

export interface Props {
  navigation?: any;
  getLessons?: any;
  setLesson?: any;
  lessons?:any;
  user?: any;
}

class DashboardComponent extends React.Component<Props, any> {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.getLessons();
  }

  render() {
    const {lessons} = this.props;
    const { data } = lessons || {} as any;
    console.log({data})
    return (
      <SafeAreaView>
        <ContentWrapper behavior="padding">
          <SContent
            contentContainerStyle={{
              flex: 1,
              justifyContent: "space-between",
              paddingLeft: 5,
              paddingRight: 5
              
            }}
          >
            <Fragment>
                {
                  !isEmpty(data) &&
                  data.map((rc,index)=> {
                    return(
                      <TouchableWithoutFeedback 
                        key = {index}
                        onPress= {() =>{ 
                          this.props.setLesson(rc);
                        
                        this.props.navigation.navigate('WebView',rc);
                        //this.props.navigation.navigate('Lesson');
                        }}
                        >
                        <Item>
                          <View>
                            <Pie
                              radius={40}
                              innerRadius={35}
                              sections={[
                                {
                                  percentage: rc.percent,
                                  color: styles.colors.orange1,
                                },
                              ]}
                              backgroundColor={styles.colors.grey3}
                            />
                            <Pic 
                              style={
                                rc.percent == 100 ?
                                {
                                  borderWidth: 5,
                                  borderColor: 'green'
                                } : {}
                              }
                              source={{uri: rc.image}} 
                            />
                          </View>
                          <TextSection>
                            <Title>{rc.quizTitle}</Title>
                            <View style={{flex: 1}}>
                              <HTML html={rc.quizSynopsis}/>
                            </View>
                            {/* <Percent>{`${rc.percent}%`}</Percent> */}
                            <View style={{flexDirection: 'row-reverse', marginTop: 20}}>
                              <TimeBlock>
                                <Image source={Assets.common.clock()}/>
                                <Label fontSize={12} color={styles.colors.grey1}>  {rc.time} minutes</Label>
                              </TimeBlock>
                            </View>
                          </TextSection>
                        </Item>
                      </TouchableWithoutFeedback>
                    )
                  })
                }
                <View style={{height:95}}/>
            </Fragment>
          </SContent>
        </ContentWrapper>
      </SafeAreaView>
    );
  }
}

const mapDispatchToProps = {
  getLessons,
  setLesson
};

const mapPropstoProps = state => ({
  user: state.common.user,
  lessons: state.common.lessons
});

const Dashboard = connect(
  mapPropstoProps,
  mapDispatchToProps
)(DashboardComponent);

export { Dashboard };
